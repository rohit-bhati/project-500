import React from 'react'
import ReactDOM from 'react-dom'
import 'bootstrap/dist/css/bootstrap.css'
import './assets/css/LineIcons.2.0.css'
import './assets/css/animate.css'
import './assets/css/tiny-slider.css'
import './assets/css/glightbox.min.css'
import './assets/css/main.css'

// import './assets/js/count-up.min'
// import './assets/js/wow.min'
// import './assets/js/tiny-slider'
// import './assets/js/glightbox.min'
// import './assets/js/imagesloaded.min'
// import './assets/js/isotope.min'
import 'bootstrap/dist/js/bootstrap.bundle'
import './assets/js/main'
import App from './App'
import reportWebVitals from './reportWebVitals'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
