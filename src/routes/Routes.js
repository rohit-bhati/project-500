import { Route, Switch } from 'react-router-dom'
import About from '../pages/About'
import Blog from '../pages/Blog'
import BlogSingle from '../pages/BlogSingle'
import Contact from '../pages/Contact'
import Home from '../pages/Home'

function Routes() {
  return (
    <Switch>
      <Route exact path='/'>
        <Home />
      </Route>
      <Route path='/about'>
        <About />
      </Route>
      <Route path='/blog'>
        <Blog />
      </Route>
      <Route path='/blog/:id'>
        <BlogSingle />
      </Route>
      <Route path='/contact'>
        <Contact />
      </Route>
    </Switch>
  )
}

export default Routes
