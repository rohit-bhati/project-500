import { Fragment } from 'react'

import aboutImg from '../assets/images/about/about-img.png'
import servicePattern from '../assets/images/service/service-patern.png'
import Breadcrumb from '../components/Breadcrumb'

function About() {
  return (
    <Fragment>
      <Breadcrumb
        title='About Us'
        links={[{ text: 'Home', to: '/' }, { text: 'About Us' }]}
      >
        <p>
          Business plan draws on a wide range of knowledge from different
          business
          <br /> disciplines. Business draws on a wide range of different
          business .
        </p>
      </Breadcrumb>
      <section className='about-us section'>
        <div className='container'>
          <div className='row'>
            <div className='col-lg-6 col-12'>
              <div className='about-left'>
                <div className='section-title align-left'>
                  <span className='wow fadeInDown' data-wow-delay='.2s'>
                    What we do
                  </span>
                  <h2 className='wow fadeInUp' data-wow-delay='.4s'>
                    Websites that tell your brand's story
                  </h2>
                  <p className='wow fadeInUp' data-wow-delay='.6s'>
                    We're a digital product and UX agency Strategy, design and
                    development across all platforms.
                  </p>
                </div>
                <div className='about-tab wow fadeInUp' data-wow-delay='.4s'>
                  <ul className='nav nav-tabs' id='myTab' role='tablist'>
                    <li className='nav-item'>
                      <a
                        className='nav-link active'
                        data-toggle='tab'
                        href='#t-tab1'
                        role='tab'
                      >
                        Content
                      </a>
                    </li>
                    <li className='nav-item'>
                      <a
                        className='nav-link'
                        data-toggle='tab'
                        href='#t-tab2'
                        role='tab'
                      >
                        Strategy
                      </a>
                    </li>
                    <li className='nav-item'>
                      <a
                        className='nav-link'
                        data-toggle='tab'
                        href='#t-tab3'
                        role='tab'
                      >
                        Development
                      </a>
                    </li>
                  </ul>
                  <div className='tab-content' id='myTabContent'>
                    <div
                      className='tab-pane fade show active'
                      id='t-tab1'
                      role='tabpanel'
                    >
                      <div className='tab-content'>
                        <p>
                          Duis aute irure dolor in reprehenderit in voluptate
                          velit esse cillum dolore eu fugiat nulla .Nemo en
                          ipsam voluptatem quia voluptas sit asper.
                        </p>
                        <ul>
                          <li>
                            <i className='lni lni-checkmark-circle'></i>{' '}
                            Commitment to excelence
                          </li>
                          <li>
                            <i className='lni lni-checkmark-circle'></i> Clients
                            are our partners
                          </li>
                          <li>
                            <i className='lni lni-checkmark-circle'></i> Fun is
                            an absolute must
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className='tab-pane fade' id='t-tab2' role='tabpanel'>
                      <div className='tab-content'>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing ,
                          sed do eiusmod tempor incididunt ut labore et dolore.
                          Ut enim ad minim veniam, quis nostrud exercitation
                          ullamco laboris nisi ut aliquip ex ea commodo
                          consequat.
                        </p>
                        <p>
                          Duis aute irure dolor in reprehenderit in voluptate
                          velit esse cillum dolore eu fugiat nulla .Nemo en
                          ipsam voluptatem quia voluptas sit asper.
                        </p>
                      </div>
                    </div>
                    <div className='tab-pane fade' id='t-tab3' role='tabpanel'>
                      <div className='tab-content'>
                        <p>
                          Duis aute irure dolor in reprehenderit in voluptate
                          velit esse cillum dolore eu fugiat nulla .Nemo en
                          ipsam voluptatem quia voluptas sit asper.
                        </p>
                        <ul>
                          <li>
                            <i className='lni lni-checkmark-circle'></i>{' '}
                            Commitment to excelence
                          </li>
                          <li>
                            <i className='lni lni-checkmark-circle'></i> Clients
                            are our partners
                          </li>
                          <li>
                            <i className='lni lni-checkmark-circle'></i> Fun is
                            an absolute must
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-lg-6 col-12'>
              <div className='about-right wow fadeInRight' data-wow-delay='.4s'>
                <img src={aboutImg} alt='#' />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='services section'>
        <div className='container'>
          <div className='row'>
            <div className='col-12'>
              <div className='section-title align-left'>
                <span className='wow fadeInDown' data-wow-delay='.2s'>
                  Care Features
                </span>
                <h2 className='wow fadeInUp' data-wow-delay='.4s'>
                  Provide Awesome Service With Our Tools
                </h2>
                <p className='wow fadeInUp' data-wow-delay='.6s'>
                  There are many variations of passages of Lorem Ipsum
                  available, but the majority have suffered alteration in some
                  form.
                </p>
              </div>
            </div>
          </div>
          <div className='single-head'>
            <img
              className='service-patern wow fadeInLeft'
              data-wow-delay='.4s'
              src={servicePattern}
              alt='#'
            />
            <div className='row'>
              <div className='col-lg-3 col-md-6 col-12'>
                <div
                  className='single-service wow fadeInUp'
                  data-wow-delay='.2s'
                >
                  <h3>
                    <a href='service-single.html'>
                      Discover, Explore the Product
                    </a>
                  </h3>
                  <div className='icon'>
                    <i className='lni lni-microscope'></i>
                  </div>
                  <p>Discover, Explore & Understanding The Product</p>
                </div>
              </div>
              <div className='col-lg-3 col-md-6 col-12'>
                <div
                  className='single-service wow fadeInUp'
                  data-wow-delay='.4s'
                >
                  <h3>
                    <a href='service-single.html'>
                      Art Direction & Brand Strategy
                    </a>
                  </h3>
                  <div className='icon'>
                    <i className='lni lni-blackboard'></i>
                  </div>
                  <p>Art Direction & Brand Communication</p>
                </div>
              </div>
              <div className='col-lg-3 col-md-6 col-12'>
                <div
                  className='single-service wow fadeInUp'
                  data-wow-delay='.6s'
                >
                  <h3>
                    <a href='service-single.html'>
                      Product UX, Design & Development‎‎
                    </a>
                  </h3>
                  <div className='icon'>
                    <i className='lni lni-ux'></i>
                  </div>
                  <p>Digital Product UX, Design & Development</p>
                </div>
              </div>
              <div className='col-lg-3 col-md-6 col-12'>
                <div
                  className='single-service wow fadeInUp'
                  data-wow-delay='.8s'
                >
                  <h3>
                    <a href='service-single.html'>
                      Marketing Strategy & SEO Campaigns
                    </a>
                  </h3>
                  <div className='icon'>
                    <i className='lni lni-graph'></i>
                  </div>
                  <p>Marketing Strategy & SEO Campaigns</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  )
}

export default About
