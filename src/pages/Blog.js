import { Fragment, useEffect, useState } from 'react'
import BlogGrid from '../components/blog/BlogGrid'

import Breadcrumb from '../components/Breadcrumb'

function Blog() {
  const [blogs, setBlogs] = useState([])

  const getData = async () => {
    const res = await fetch('./resources/data/blogs.json')
    const blogs = await res.json()
    setBlogs(blogs)
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <Fragment>
      <Breadcrumb
        title='Blog'
        links={[{ text: 'Home', to: '/' }, { text: 'Blog' }]}
      >
        <p>
          Business plan draws on a wide range of knowledge from different
          business
          <br /> disciplines. Business draws on a wide range of different
          business .
        </p>
      </Breadcrumb>

      <section className='section latest-news-area blog-list'>
        <div className='container'>
          <div className='row'>
            <div className='col-lg-8 col-md-7 col-12'>
              <div className='row'>
                {blogs.map((blog) => (
                  <BlogGrid blog={blog} key={blog.id} />
                ))}
                {/* <BlogGrid blogs={blogs} /> */}
              </div>

              <div className='pagination center'>
                <ul className='pagination-list'>
                  <li>
                    <a href='/'>
                      <i className='lni lni-chevron-left'></i>
                    </a>
                  </li>
                  <li className='active'>
                    <a href='/'>1</a>
                  </li>
                  <li>
                    <a href='/'>2</a>
                  </li>
                  <li>
                    <a href='/'>3</a>
                  </li>
                  <li>
                    <a href='/'>4</a>
                  </li>
                  <li>
                    <a href='/'>
                      <i className='lni lni-chevron-right'></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <aside className='col-lg-4 col-md-5 col-12'>
              <div className='sidebar'>
                <div className='widget search-widget'>
                  <h5 className='widget-title'>Search Objects</h5>
                  <form action='#'>
                    <input type='text' placeholder='Search Here...' />
                    <button type='submit'>
                      <i className='lni lni-search-alt'></i>
                    </button>
                  </form>
                </div>
                <div className='widget popular-feeds'>
                  <h5 className='widget-title'>Popular Feeds</h5>
                  <div className='popular-feed-loop'>
                    <div className='single-popular-feed'>
                      <div className='feed-desc'>
                        <h6 className='post-title'>
                          <a href='/'>
                            8 simple ways to utilize a blog to improve SEO
                            results
                          </a>
                        </h6>
                        <span className='time'>
                          <i className='lni lni-calendar'></i> 05th Nov 2023
                        </span>
                      </div>
                    </div>
                    <div className='single-popular-feed'>
                      <div className='feed-desc'>
                        <h6 className='post-title'>
                          <a href='/'>
                            7 most important SEO focus areas for colleges and
                            universities
                          </a>
                        </h6>
                        <span className='time'>
                          <i className='lni lni-calendar'></i> 24th March 2023
                        </span>
                      </div>
                    </div>
                    <div className='single-popular-feed'>
                      <div className='feed-desc'>
                        <h6 className='post-title'>
                          <a href='/'>
                            How to drive conversions with on-brand SEO
                            copywriting
                          </a>
                        </h6>
                        <span className='time'>
                          <i className='lni lni-calendar'></i> 30th Jan 2023
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='widget categories-widget'>
                  <h5 className='widget-title'>Categories</h5>
                  <ul className='custom'>
                    <li>
                      <a href='/'>
                        Business<span>26</span>
                      </a>
                    </li>
                    <li>
                      <a href='/'>
                        Consultant<span>30</span>
                      </a>
                    </li>
                    <li>
                      <a href='/'>
                        Creative<span>71</span>
                      </a>
                    </li>
                    <li>
                      <a href='/'>
                        UI/UX<span>56</span>
                      </a>
                    </li>
                    <li>
                      <a href='/'>
                        Technology<span>60</span>
                      </a>
                    </li>
                  </ul>
                </div>
                <div className='widget popular-tag-widget'>
                  <h5 className='widget-title'>Popular Tags</h5>
                  <div className='tags'>
                    <a href='/'>Popular Template</a>
                    <a href='/'>Design</a>
                    <a href='/'>UX</a>
                    <a href='/'>Icon</a>
                    <a href='/'>Usability</a>
                    <a href='/'>Tech</a>
                    <a href='/'>Mouse</a>
                    <a href='/'>Kit</a>
                    <a href='/'>Consult</a>
                    <a href='/'>Business</a>
                    <a href='/'>Keyboard</a>
                    <a href='/'>Develop</a>
                  </div>
                </div>
              </div>
            </aside>
          </div>
        </div>
      </section>
    </Fragment>
  )
}

export default Blog
