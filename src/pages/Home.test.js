import { render, screen } from '@testing-library/react'
import Home from './Home'

it('renders discover more text', () => {
  render(<Home />)
  expect(screen.getByText('Discover More')).toBeInTheDocument()
})
