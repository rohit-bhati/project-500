import { BrowserRouter as Router } from 'react-router-dom'
import Layout from './components/Layout'
import Routes from './routes/Routes'

function App() {
  return (
    <div>
      <Router>
        <Layout>
          <Routes />
        </Layout>
      </Router>
    </div>
  )
}

export default App
