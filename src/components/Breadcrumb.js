import { Fragment } from 'react'
import { Link } from 'react-router-dom'

function Breadcrumb({ links, title, children }) {
  return (
    <Fragment>
      <div className='breadcrumbs'>
        <div className='container'>
          <div className='row'>
            <div className='col-lg-8 offset-lg-2 col-12'>
              <div className='breadcrumbs-content'>
                <h1 className='page-title'>{title}</h1>
                {children}
              </div>
              <ul className='breadcrumb-nav'>
                {links.map((link) => (
                  <li key={link.text}>
                    {!link.hasOwnProperty('to') ? (
                      link.text
                    ) : (
                      <Link to={link.to}>{link.text}</Link>
                    )}
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default Breadcrumb
