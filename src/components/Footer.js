import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import footerLogo from '../assets/images/logo/footer-logo.svg'

const Footer = () => {
  return (
    <Fragment>
      <footer className='footer'>
        <div className='footer-middle'>
          <div className='container'>
            <div className='row'>
              <div className='col-lg-3 col-md-6 col-12'>
                <div className='f-about single-footer'>
                  <div className='logo'>
                    <a href='/'>
                      <img src={footerLogo} alt='header logo' />
                    </a>
                  </div>
                  <p>
                    Start building your creative website with our awesome
                    template Massive.
                  </p>
                  <div className='footer-social'>
                    <ul>
                      <li>
                        <a href='https://instagram.com'>
                          <i className='lni lni-instagram'></i>
                        </a>
                      </li>
                      <li>
                        <a href='https://twitter.com'>
                          <i className='lni lni-twitter'></i>
                        </a>
                      </li>
                      <li>
                        <a href='https://linkedin.com'>
                          <i className='lni lni-linkedin'></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className='col-lg-3 col-md-6 col-12'>
                <div className='single-footer f-link'>
                  <h3>Pages</h3>
                  <ul>
                    <li>
                      <Link to='/'>About Us</Link>
                    </li>
                    <li>
                      <Link to='/'>Our Services</Link>
                    </li>
                    <li>
                      <Link to='/'>Contact Us</Link>
                    </li>
                    <li>
                      <Link to='/'>Our Blog</Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className='col-lg-3 col-md-6 col-12'>
                <div className='single-footer f-link'>
                  <h3>More Links</h3>
                  <ul>
                    <li>
                      <Link to='/'>Topics</Link>
                    </li>
                    <li>
                      <Link to='/'>Terms of Use</Link>
                    </li>
                    <li>
                      <Link to='/'>Privacy Policy</Link>
                    </li>
                    <li>
                      <Link to='/'>License</Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className='col-lg-3 col-md-6 col-12'>
                <div className='single-footer f-link'>
                  <h3>Support</h3>
                  <ul>
                    <li>
                      <Link to='/'>Cookies</Link>
                    </li>
                    <li>
                      <Link to='/'>Forum</Link>
                    </li>
                    <li>
                      <Link to='/'>Support Team</Link>
                    </li>
                    <li>
                      <Link to='/'>Sitemap</Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='footer-bottom'>
          <div className='container'>
            <div className='inner'>
              <div className='row'>
                <div className='col-12'>
                  <div className='left'>
                    <p>
                      Designed and Developed by
                      <a
                        href='https://graygrids.com/'
                        rel='noreferrer'
                        target='_blank'
                      >
                        GrayGrids
                      </a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </Fragment>
  )
}

export default Footer
