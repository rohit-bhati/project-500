import React, { Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import logo from '../assets/images/logo/logo.svg'

const Header = () => {
  return (
    <Fragment>
      <header className='header'>
        <div className='navbar-area sticky'>
          <div className='container'>
            <div className='row align-items-center'>
              <div className='col-lg-12'>
                <nav className='navbar navbar-expand-lg'>
                  <a href='/' className='navbar-brand logo'>
                    <img src={logo} alt='header logo' />
                  </a>
                  <button
                    className='navbar-toggler'
                    type='button'
                    data-toggle='collapse'
                    data-target='#navbarSupportedContent'
                    aria-controls='navbarSupportedContent'
                    aria-expanded='false'
                    aria-label='Toggle navigation'
                  >
                    <span className='toggler-icon'></span>
                    <span className='toggler-icon'></span>
                    <span className='toggler-icon'></span>
                  </button>
                  <div
                    className='collapse navbar-collapse sub-menu-bar'
                    id='navbarSupportedContent'
                  >
                    <ul id='nav' className='navbar-nav ms-auto'>
                      <li className='nav-item'>
                        <NavLink exact to='/' activeClassName='active'>
                          Home
                        </NavLink>
                      </li>
                      <li className='nav-item'>
                        <NavLink to='/about' activeClassName='active'>
                          About Us
                        </NavLink>
                      </li>
                      <li className='nav-item'>
                        <NavLink to='/blog'>Blog</NavLink>
                      </li>
                      <li className='nav-item'>
                        <NavLink to='/contact'>Contact</NavLink>
                      </li>
                    </ul>
                  </div>
                  <div className='button'>
                    <NavLink to='/contact' className='btn'>
                      Get it now
                    </NavLink>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </header>
    </Fragment>
  )
}

export default Header
