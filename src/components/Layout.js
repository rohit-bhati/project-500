import Footer from './Footer'
import Header from './Header'

const Layout = ({ children }) => {
  return (
    <div>
      <div className='preloader'>
        <div className='preloader-inner'>
          <div className='preloader-icon'>
            <span></span>
            <span></span>
          </div>
        </div>
      </div>
      <Header />
      {children}
      <Footer />
      {/* <a href='#' className='scroll-top btn-hover'>
        <i className='lni lni-chevron-up'></i>
      </a> */}
      <button
        onClick={() => window.scrollTo(0, 0)}
        className='scroll-top btn-hover'
      >
        <i className='lni lni-chevron-up'></i>
      </button>
    </div>
  )
}

export default Layout
