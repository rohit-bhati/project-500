import { Fragment } from 'react'
import author from '../../assets/images/blog/author.jpg'
import blogGrid1 from '../../assets/images/blog/blog-grid1.jpg'
import blogGrid2 from '../../assets/images/blog/blog-grid2.jpg'
import blogGrid3 from '../../assets/images/blog/blog-grid3.jpg'

function BlogGrid({ blog }) {
  const randomImage = (img_url) => {
    const images = {
      'blog-grid1': blogGrid1,
      'blog-grid2': blogGrid2,
      'blog-grid3': blogGrid3
    }

    return images[img_url]
  }

  return (
    <Fragment>
      <div className='col-lg-6 col-12'>
        <div
          className='single-news wow fadeInUp'
          data-wow-delay='.2s'
          style={{
            visibility: 'visible',
            animationDelay: '0.2s',
            animationName: 'fadeInUp'
          }}
        >
          <div className='image'>
            <img className='thumb' src={randomImage(blog.img_url)} alt='#' />
            <div className='meta-details'>
              <img src={author} alt='#' /> <span>{blog.author.name}</span>
            </div>
          </div>
          <div className='content-body'>
            <h4 className='title'>
              <a href='blog-single-sidebar.html'>{blog.title}</a>
            </h4>
            <p>{blog.description}</p>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default BlogGrid
